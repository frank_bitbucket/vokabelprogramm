﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;

namespace ConsoleApp37
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                //Console.ReadKey();
                
                string sourceLanguage;
                string targetLanguage;
                string sourceVocab;
                string translatedVocab;
                var username = "";
                var sqliteUserTableName = "";

                Console.Write("What's your name?");
                username = Console.ReadLine();
                sqliteUserTableName = "vocabulary_" + username;
                Console.WriteLine("What do you want to do?");
                Console.WriteLine("1_Create a new vocabulary database entry?");
                Console.WriteLine("2_Import a vocab file?");
                Console.WriteLine("3_Learn vocabulary?");
                Console.WriteLine("4_Count number of DB-Records");
                Console.WriteLine("5_Read all records from database");
                Console.WriteLine("6_Delete single vocab from database");
                Console.WriteLine("7_Export all vocabs from database");
                Console.WriteLine("8_Search a vocab");
                Console.WriteLine("9_Update the user-vocabulary-table");
                Console.WriteLine("Q_Quit");


                char choice = char.Parse(Console.ReadLine());
                int retval = 0;
                string stm = "";
                string cs = "URI=file:c:/C#/vokabelprogramm/Database/VokabelDB.s3db";
                SQLiteConnection con = new SQLiteConnection(cs);
                SQLiteCommand cmd = new SQLiteCommand(stm, con);
                //SQLiteDataReader reader = cmd.ExecuteReader(); //Ergebnis abrufen

                switch (choice)
                {

                    case '1':
                        #region Case1 - write a vocabulary in the database

                        DateTime today = DateTime.Now;
                        string myTime = System.DateTime.Now.ToString();
                        myTime = myTime.Remove(0, 11);
                        
                        
                        con.Open();
                        
                        
                        Console.Write("Source language: ");
                        sourceLanguage = Console.ReadLine();
                        Console.Write("Target language: ");
                        targetLanguage = Console.ReadLine();
                        Console.Write("Vocab: ");
                        sourceVocab = Console.ReadLine();
                        Console.Write("Translation of the vocab: ");
                        translatedVocab = Console.ReadLine();

                        //stm = @"Insert into vocabulary([Source_Language],[Target_Language],[Source_Vocab],[Target_Vocab],[Import_Date],[Import_Time],[Ignore_Vocab],[Learn_Level],[Last_Check],[Days_till_next_Check],Counter)Values";
                        //stm += "('" + sourceLanguage + "','" + targetLanguage + "','" + sourceVocab + "','" + translatedVocab + "',DateTime('now')" + ",'" + myTime + "',0,1,'',0,0)";

                        stm = @"Insert into vocabulary([Source_Language],[Target_Language],[Source_Vocab],[Target_Vocab],[Import_Date],[Import_Time],[Imported_by])Values";
                        stm += "('" + sourceLanguage + "','" + targetLanguage + "','" + sourceVocab + "','" + translatedVocab + "',DateTime('now')" + ",'" + myTime + "','" + username + "')";
                        try
                        {
                            cmd = new SQLiteCommand(stm, con);
                            retval = cmd.ExecuteNonQuery();


                            if (retval == 1)
                                Console.WriteLine("Row inserted!");
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("\nMessage ---\n{0}", ex.Message);
                            Console.WriteLine("Row NOT inserted.");
                        }


                        con.Close();
                        break;
                    #endregion

                    case '2':
                        #region Case2 - import a csv-file with vocabulary records                                       
                        sourceLanguage = "";
                        targetLanguage = "";

                        //string filePath = @"c:\C#\VokabelProgramm\Vokabeln\VokabelTest.csv";
                        //string filePath = @"c:\C#\VokabelProgramm\Vokabeln\Vokabel_2.csv";
                        //string filePath = @"c:\C#\VokabelProgramm\Vokabeln\Vokabel_1.csv
                        Console.WriteLine("Please enter the vocab file name:");
                        string vocabFileName = Console.ReadLine();

                        string filePath = @"c:\C#\VokabelProgramm\Vokabeln\" + vocabFileName;
                        try
                        {


                            StreamReader sr = new StreamReader(filePath, Encoding.GetEncoding("UTF-8"));
                            cs = "URI=file:c:/C#/vokabelprogramm/Database/VokabelDB.s3db";
                            con = new SQLiteConnection(cs);
                            con.Open();
                            int Row = 0;
                            Console.Write("Source language: ");
                            sourceLanguage = Console.ReadLine();
                            Console.Write("Target language: ");
                            targetLanguage = Console.ReadLine();

                            DateTime _dtStart = DateTime.Now;
                            
                            while (!sr.EndOfStream)
                            {
                                try
                                {
                                    string[] Line = sr.ReadLine().Split('|');
                                    myTime = System.DateTime.Now.ToString();
                                    myTime = myTime.Remove(0, 11);

                                    stm = @"Insert into vocabulary([Source_Language],[Target_Language],[Source_Vocab],[Target_Vocab],[Import_Date],[Import_Time],[Imported_by])Values";
                                    stm += "('" + sourceLanguage + "','" + targetLanguage + "','" + @Line[1].Replace("'", "''") + "','" + @Line[0].Replace("'", "''") + "',DateTime('now')" + ",'" + myTime + "','" + username + "')";

                                    cmd = new SQLiteCommand(stm, con);
                                    retval = cmd.ExecuteNonQuery();
                                    Console.WriteLine("Vocab '{0} - {1}' has been imported!", Line[1], Line[0]);

                                    Row++;
                                    //Console.WriteLine(Row); }

                                }
                                catch
                                {

                                    //Console.WriteLine("Fehler");
                                };

                            }
                            DateTime _dtEnde = DateTime.Now;
                            int dauer = (int)(_dtEnde - _dtStart).TotalMinutes;
                            Console.WriteLine("Anfang: {0} Ende: {1}", _dtStart, _dtEnde);
                            Console.WriteLine("Import has been done! Duration: {0}", dauer);
                            ///




                        }
                        catch (Exception ex)
                        {

                            Console.WriteLine("\nMessage ---\n{0}", ex.Message);
                            /*
                            Console.WriteLine(
                                "\nHelpLink ---\n{0}", ex.HelpLink);
                            Console.WriteLine("\nSource ---\n{0}", ex.Source);
                            Console.WriteLine(
                                "\nStackTrace ---\n{0}", ex.StackTrace);
                            Console.WriteLine(
                                "\nTargetSite ---\n{0}", ex.TargetSite);
                            */
                        }
                        break;
                    #endregion

                    case '3':
                        #region Case3 - Learn vocabulary

                        Console.WriteLine("How many vocabs do you want to learn today?");
                        int NumberOfVocabsToLearn = Convert.ToInt32(Console.ReadLine());
                        //string[] tempVocab = new string[,,,,];
                       

                        stm = "Select * from vocabulary where [Learn_Level] = 1 AND [Last_Check] = '' Limit " + NumberOfVocabsToLearn;
                        con = new SQLiteConnection(cs);
                        con.Open();
                        cmd = new SQLiteCommand(stm, con);
                        SQLiteDataReader reader = cmd.ExecuteReader(); //Ergebnis abrufen
                        var i = 0; 
                        while (reader.Read())
                        {
                            Console.WriteLine("DE: " + reader[2] + " EN: " + reader[3]); 
                            //tempVocab[i,2,3,] 

                        }


                        con.Close(); //Verbindung zur Datenbank trennen

                        //break;
                        continue;
                    #endregion;

                    case '4':
                        #region Case4 - Count DB-Records
                        //reader = cmd.SendReturnSQLCommand(con, "SELECT COUNT(*) AS rec_count FROM table WHERE field = 'some_value';");
                        stm = "Select Count(*) AS rec_count from vocabulary";
                        con = new SQLiteConnection(cs);
                        con.Open();
                        cmd = new SQLiteCommand(stm, con);
                        reader = cmd.ExecuteReader(); //Ergebnis abrufen

                        int count = 0;

                        if (reader.HasRows)
                        {
                            reader.Read();
                            count = Convert.ToInt32(reader["rec_count"].ToString());
                            //break;
                        }
                        Console.WriteLine("Anzahl: {0}", Convert.ToString(count));
                        Console.ReadKey();
                        Console.Clear();
                        
                        continue;
                        //break;
                      
                    #endregion

                    case '5':
                        #region Case5 - show all entries of the DB
                        
                        con = new SQLiteConnection(cs);
                        con.Open();
                        stm = "Select * from vocabulary";
                        cmd = new SQLiteCommand(stm, con);
                        reader = cmd.ExecuteReader(); //Ergebnis abrufen

                        //retval = cmd.ExecuteNonQuery();
                        while (reader.Read())
                        {
                            Console.WriteLine("DE: " + reader[2] + " EN: " + reader[3]); // + " Vorname: " + reader[2]);
                        }
                        con.Close(); //Verbindnung zur Datenbank trennen

                        break;
                    #endregion

                    case '6':
                        #region Case6 - delete single vocabs from the database
                        con = new SQLiteConnection(cs);
                        con.Open();
                        Console.WriteLine("Which vocab do you want to delete from the database?");
                        string vocabToDelete = Console.ReadLine();
                        Console.Write("Source language: ");
                        sourceLanguage = Console.ReadLine();
                        Console.Write("Target language: ");
                        targetLanguage = Console.ReadLine();
                        //SQLiteDataReader reader = cmd.ExecuteReader();
                        try
                        {
                            stm = "select Count(*) AS rec_count from vocabulary where [Source_Language] = '" + sourceLanguage + "' AND [Target_Language] = '" + targetLanguage + "' AND [Source_Vocab] = ";
                            stm += "'" + vocabToDelete + "'";
                            count = 0;
                            cmd = new SQLiteCommand(stm, con);
                            reader = cmd.ExecuteReader();

                            if (reader.HasRows)
                            {
                                reader.Read();
                                count = Convert.ToInt32(reader["rec_count"].ToString());
                                //break;
                            }
                            if (count > 0)
                            {
                                Console.WriteLine("The vocab '{0} - {1}' exists. Do you really want to delete it y/n?", vocabToDelete);
                                char deletionConfirmation = char.Parse(Console.ReadLine());
                                switch (deletionConfirmation)
                                {

                                    case 'y':

                                        stm = "delete from vocabulary where [Source_Language] = '" + sourceLanguage + "' AND [Target_Language] = '" + targetLanguage + "' AND [Source_Vocab] = ";
                                        stm += "'" + vocabToDelete + "'";
                                        Console.WriteLine(stm);
                                        cmd = new SQLiteCommand(stm, con);
                                        reader = cmd.ExecuteReader(); //Ergebnis abrufen
                                        Console.WriteLine("Vocab '{0}' has been deleted!", vocabToDelete);
                                        break;
                                    case 'n':
                                        break;
                                };
                            }


                            else
                            {
                                Console.WriteLine("The vocab '{0}' doesn't exist!", vocabToDelete);
                            }
                            /*

                            */
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("xxx\nMessage ---\n{0}", ex.Message);
                        };

                        continue;
                    //break;
                    #endregion

                    case '7':
                        #region Case7 - Export all vocabs
                        Console.Write("Source language: ");
                        sourceLanguage = Console.ReadLine();
                        Console.Write("Target language: ");
                        targetLanguage = Console.ReadLine();
                        Console.Write("Please specify the separator sign ");
                        var separator = Console.ReadLine();
                        Console.WriteLine("Please enter a place and name for the exportfile ");
                        var targetFileName = Console.ReadLine();
                        stm = "Select * from vocabulary where [Source_Language] = '" + sourceLanguage + "' AND [Target_Language] = '" + targetLanguage + "'";
                        con = new SQLiteConnection(cs);
                        con.Open();
                        cmd = new SQLiteCommand(stm, con);
                        reader = cmd.ExecuteReader(); //Ergebnis abrufen
                        //StreamWriter sw = new StreamWriter(@"C:\Users\Frank\Desktop\Temp\C#\Test.txt", true);
                        StreamWriter sw = new StreamWriter(targetFileName, true);
                        sw.WriteLine("");
                        
                        while (reader.Read())
                        {
                            //Console.WriteLine("DE: " + reader[2] + " EN: " + reader[3]); // + " Vorname: " + reader[2]);
                            //sw.WriteLine(reader[0] + ";" + reader[1] + ";" + reader[2] + ";" + reader[3]);
                            sw.WriteLine(reader[0] + separator + reader[1] + separator + reader[2] + separator + reader[3]);

                        }
                        sw.Close();
                        con.Close(); //Verbindung zur Datenbank trennen
                        Console.WriteLine("Export done");
                        break;

                    //sw.Close();
                    #endregion

                    case '8':
                        #region Case8 - Search for a vocab
                        Console.WriteLine("Please enter the vocab you search for:");
                        var vocabString = Console.ReadLine();
                        Console.Write("Source language: ");
                        sourceLanguage = Console.ReadLine();
                        try
                        {
                            stm = "Select * from vocabulary where [Source_Language] = '" + sourceLanguage + "' AND [Source_Vocab] like '%" + vocabString + "%'";
                            //Console.WriteLine(stm);
                            //break;
                            con = new SQLiteConnection(cs);
                            con.Open();
                            cmd = new SQLiteCommand(stm, con);
                            reader = cmd.ExecuteReader();
                            var Counter = 1;
                            while (reader.Read())
                            {
                                Console.WriteLine(Convert.ToString(Counter) + " - " + reader[2] + " - " + reader[3]);
                                //Console.WriteLine("DE: " + reader[2] + " EN: " + reader[3]); 
                                Counter += 1;

                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("xxx\nMessage ---\n{0}", ex.Message);
                        };





                        break;
                    #endregion

                    case '9':
                        #region Case9 - update user-database
                        Console.WriteLine("Do you want to update the user-vocabular_table {0}?", sqliteUserTableName);
                        Console.Write("y / n");
                        string updateUserTableChoice = Console.ReadLine();
                        updateUserTableChoice = updateUserTableChoice.ToUpper();
                        
                        if (updateUserTableChoice == "Y") 
                            {
                            con = new SQLiteConnection(cs);
                            con.Open();
                            stm = "Select * from vocabulary";
                            cmd = new SQLiteCommand(stm, con);
                            reader = cmd.ExecuteReader(); //Ergebnis abrufen
                            var stm_update = "";

                            while (reader.Read())
                            {
                                //Console.WriteLine("DE: " + reader[2] + " EN: " + reader[3]); // + " Vorname: " + reader[2]);
                                //stm = @"Insert into vocabulary([Source_Language],[Target_Language],[Source_Vocab],[Target_Vocab],[Import_Date],[Import_Time],[Ignore_Vocab],[Learn_Level],[Last_Check],[Days_till_next_Check],Counter)Values";
                                //stm += "('" + sourceLanguage + "','" + targetLanguage + "','" + sourceVocab + "','" + translatedVocab + "',DateTime('now')" + ",'" + myTime + "',0,1,'',0,0)";

                                stm_update = @"Insert into " + sqliteUserTableName + "([Source_Language],[Target_Language],[Source_Vocab],[Target_Vocab],[Ignore_Vocab],[Learn_Level],[Last_Check],[Days_till_next_Check],[User])Values";
                                stm_update += "('" + reader[0] + "','" + reader[1] + "','" + reader[2] + "','" + reader[3] + "',0,1,'',0,'" + username + "')";
                                //reader = cmd.ExecuteReader();
                                //Console.WriteLine(stm_update);
                                //break;

                                try
                                {
                                    cmd = new SQLiteCommand(stm_update, con);
                                    retval = cmd.ExecuteNonQuery();


                                    if (retval == 1)
                                        Console.WriteLine("Row inserted!");
                                    else
                                        break;
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("\nMessage ---\n{0}", ex.Message);
                                    Console.WriteLine("Row NOT inserted.");
                                }



                            }
                            con.Close(); //Verbindnung zur Datenbank trennen
                        };
                        

                        break;



                    #endregion

                    case 'Q':
                    case 'q':
                        #region Quit programm
                        //continue;
                        break;
                        #endregion
                }

                Console.ReadKey();

            }//xxx
            #region Testverbindung zur SQLite-DB;
            /*
            string cs = "URI=file:c:/Temp/C#/VokabelDB.s3db"; 

            using (SQLiteConnection con = new SQLiteConnection(cs))
            {
                con.Open();

                string stm = @"SELECT * From Vocabulary";


                using (SQLiteCommand cmd = new SQLiteCommand(stm, con))
                {
                    using (SQLiteDataReader rdr = cmd.ExecuteReader())
                    {
                        do
                        {
                            rdr.Read();
                            Console.WriteLine("Vokabel: {0}", rdr["Source_Vocab"]);
                            Console.WriteLine("Übersetzung: {0}", rdr["Target_Vocab"]);
                            Console.ReadKey();
                        } while (rdr.NextResult());

                    }
                }

                con.Close();
            }
            */
            #endregion
        }
        
    }
}
